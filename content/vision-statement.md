+++
title = "Our Vision"
date = 2024-06-01
description = "To cultivate an inclusive community of LGBTQIA2S+ individuals and their allies"
extra = {header_img = "/background.jpg"}
+++

To cultivate an inclusive community of LGBTQIA2S+ individuals and their allies, supporting one another, our identities, and expressions, through resources, programs, and events aimed at education, empowerment, and celebration. We hope to bring into focus the existing diversity of Chestermere and the surrounding area to improve the lives of those often marginalized based on their sexuality, gender identity, gender presentation, or other characteristics. We wish to show that the world is better when people are seen for and treated fairly as they are, without bigotry or hate.
