+++
title = "CAPS Style Guide"
date = 2024-09-06
description = "Communications and Brand Guide"
extra = {header_img = "/background.jpg"}
+++

# CAPS Society Communications and Branding Style Guide
## Helpful hints on how we can all sprëchen the same.

The purpose of the Style Guide is to help all CAPS communications and client contacts to contain the same flavour and feeling when done by different parties.

All English language communications are in Canadian English, and will follow [Strunk and White](https://www.gutenberg.org/files/37134/37134-h/37134-h.htm).

French language should use the [CTC](https://www.btb.termiumplus.gc.ca/tcdnstyl-chap?lang=eng&lettr=chapsect17&info0=17) guide and also avail themselves of [Egale](https://egale.ca/awareness/french-style-guide/) to avoid blunders of gender or identity. 

मुझे हिंदी का समर्थन करने में खुशी हो रही है


### Colours
We have a set colour palette.
Insert here.

### Webpages
Markdown et al.

### Emails
#### Signatures

Signatures should be as follows:

```
--
Name (pro/nouns)
Chestermere Area Pride Society
1234567AB Ltd.
mailto:ryan@capsociety.org
gpg:deadbeefcafe1234
https://capsociety.org
Phone:(555) 555-1212
```

Not all people will need all this info, but at the very least the double dash, 
top two lines, and url should be included.

Do not put logos or graphics in your signature.

### Printed Materials

### Geekery
Use PEP8 with all applicable. Ruff and black.

1TBS/Stroustrup style K&R for other languages.
