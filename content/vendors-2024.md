+++
title = "Vendors at Pride 2024"
date = 2024-06-01
description = "Sign up today to help us throw the best Pride ever"
extra = {header_img = "/splat_background_saturated.jpg"}
aliases = ['/en/vendors-2024.md']
+++
More vendor information will be available soon.

**Email** [**hello@capsociety.org**](volunteers@capsociety.org) **for more information until then.**
