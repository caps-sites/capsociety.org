+++
title = "CAPS Mission Statement"
date = 2024-06-01
updated = 2024-06-01
description = "CAPS is dedicated to providing a safe and supportive space for the LGBTQIA2S+ community"
extra = {header_img = "/background.jpg"}
+++

Beginning with our first event in June of 2024, the Chestermere Pride Festival, the Chestermere and Area Pride Society (CAPS) is dedicated to providing a safe and supportive space for the LGBTQIA2S+ community to come together for support, ally-ship, and personal growth. 
We strive to promote acceptance, visibility, and rights for all individuals, and create spaces for community-building initiatives and resources to foster a culture of inclusivity and belonging.
