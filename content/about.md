+++
title = "About CAPS"
date = 2024-06-01
description = "Some things to know about our organization"
extra = {header_img = "/background.jpg"}
+++

At Chestermere and Area Pride Society (CAPS) we organize events and programs geared toward bringing awareness, acceptance, and advocacy to the ever-growing LGBTQIA2S+ community. We provide a safe and inclusive space where people can explore their identities and orientations, meet and interact with other community members, learn from elders and share experiences, as well as work together to grow and foster a brighter and more loving future.
