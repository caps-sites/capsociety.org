+++
title = "Volunteer for Pride 2024"
date = 2024-06-01
updated = 2024-06-06
description = "SIGN UP IS OPEN! Help us throw the best Pride ever"
aliases = [ 'en/volunteering-2024', 'volunteering-2024-en' ]
extra = {header_img = "/splat_background_saturated.jpg"} 
+++

# Volunteer signup is now open!

Our volunteer sign-up link is here: [https://signup.com/go/PUmgNhv](https://signup.com/go/PUmgNhv)

**Email** [volunteers\@capsociety.org](mailto:volunteers@capsociety.org) **for more information**
